export const environment = {
  production: true,
  appName: 'FS Badges',
  avatarMinSize: 128,
  avatarMaxSize: 256,
  badgeMaxSize: 256,
};
