import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { XMaterialModule } from './sub-modules/x-material/x-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { LogoutComponent } from './components/logout/logout.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

import { BadgeMgtComponent, UploadCSVDialogComponent } from './components/home/badge-mgt/badge-mgt.component';
import {
  RecipientMgtComponent,
  AssignIssuerDialogComponent,
  RecipientDetailsDialogComponent
} from './components/home/recipient-mgt/recipient-mgt.component';
import { OrganizationMgtComponent } from './components/home/organization-mgt/organization-mgt.component';
import { IssuerMgtComponent } from './components/home/issuer-mgt/issuer-mgt.component';
import {
  WalletComponent,
  AssertionDialogComponent,
  ShareBottomSheetComponent,
  ShareEmailAddressComponent
} from './components/home/wallet/wallet.component';
import { BadgeComponent } from './components/home/wallet/badge/badge.component';
import { LanguagePipe } from './pipes/language.pipe';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    HomeComponent,
    NotFoundComponent,
    RecipientMgtComponent, AssignIssuerDialogComponent, RecipientDetailsDialogComponent,
    BadgeMgtComponent, UploadCSVDialogComponent,
    OrganizationMgtComponent,
    IssuerMgtComponent,
    WalletComponent, AssertionDialogComponent, ShareBottomSheetComponent, ShareEmailAddressComponent,
    BadgeComponent,
    LanguagePipe
  ],
  imports: [
    BrowserModule, AppRoutingModule, BrowserAnimationsModule, HttpClientModule,
    FormsModule, ReactiveFormsModule, XMaterialModule, FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    UploadCSVDialogComponent,
    AssignIssuerDialogComponent, RecipientDetailsDialogComponent,
    AssertionDialogComponent, ShareBottomSheetComponent,
    ShareEmailAddressComponent]
})
export class AppModule {
}
