import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';

import { MainComponent } from '../../../main.component';
import { Organization } from '../../../ds/organization';
import { Utils } from '../../../shared/utils';
import { environment as globals } from '../../../../environments/environment';

@Component({
  selector: 'app-organization-mgt',
  templateUrl: './organization-mgt.component.html',
  styleUrls: ['./organization-mgt.component.scss', '../../../shared/forms.scss', '../../../shared/table.scss']
})
export class OrganizationMgtComponent extends MainComponent implements OnInit {

  displayedColumns: string[] = ['image', 'name', 'email', 'telephone', 'url', '_id'];

  organizations: Organization[] = [];
  createForm: FormGroup;
  imageSrc: any;

  constructor(private formBuilder: FormBuilder,
              protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.createForms();
    this.getOrganizations();
  }

  getOrganizations(): void {
    this.api.organizationGet()
      .subscribe(
        (data: Organization[]) => {
          if (data.length === 0) {
            this.openSnackBar('No organizations found', 'Ok');
          }
          this.organizations = data;
          console.log(data);
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  get name(): AbstractControl {
    return this.createForm.get('name');
  }

  get url(): AbstractControl {
    return this.createForm.get('url');
  }

  get email(): AbstractControl {
    return this.createForm.get('email');
  }

  get image(): AbstractControl {
    return this.createForm.get('image');
  }

  get telephone(): AbstractControl {
    return this.createForm.get('telephone');
  }

  get description(): AbstractControl {
    return this.createForm.get('description');
  }

  private createForms(): void {
    this.createForm = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(64),
        Validators.pattern(/^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/)
      ])],
      url: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(256),
        Validators.pattern(/(^|\s)((https?:\/\/)?[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?)/i)
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      image: [''],
      telephone: ['', Validators.pattern(/[0-9]{10}/)],
      description: ['']
    });
  }

  getErrorMessage(field: string): string {
    switch (field) {
      case 'name':
        if (this.name.hasError('required')) {
          return 'Name is <strong>required</strong>';
        }
        if (this.name.hasError('minlength')) {
          return `Name must be <strong>at least ${this.name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.name.hasError('maxlength')) {
          return `Name must be <strong>less than ${this.name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> name';
      case 'url':
        if (this.url.hasError('required')) {
          return 'URL is <strong>required</strong>';
        }
        if (this.url.hasError('minlength')) {
          return `URL must be <strong>at least ${this.url.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.url.hasError('maxlength')) {
          return `URL must be <strong>less than ${this.url.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> URL';
      case 'email':
        if (this.email.hasError('required')) {
          return 'Email is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> email address';
      case 'telephone':
        return '<strong>Invalid</strong> telephone';
    }
  }

  onFileChange($event): void {
    if (($event.target as HTMLInputElement).files && ($event.target as HTMLInputElement).files[0]) {
      if (($event.target as HTMLInputElement).files[0].size < 1024 * 1024 * 5) { // 5MB
        Utils.resizeImage({file: ($event.target as HTMLInputElement).files[0], maxSize: globals.avatarMaxSize})
          .then(img => this.imageSrc = img)
          .catch(err => this.openSnackBar(err.message, 'Ok', false));
      } else {
        this.openSnackBar('Image file is too large.', 'Ok', false);
      }
    } else {
      this.imageSrc = undefined;
    }
  }

  onCreate(formData: FormGroup, formDirective: FormGroupDirective): void {
    this.api.organizationCreate(this.imageSrc ? {...formData.value, ...{image: this.imageSrc}} : formData.value)
      .subscribe(
        (data) => {
          formDirective.resetForm();
          this.createForm.reset();
          this.imageSrc = undefined;
          this.getOrganizations();
          this.openSnackBar(data.message, 'Ok', true);
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

}
