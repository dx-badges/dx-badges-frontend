import { Component, Injector, OnInit } from '@angular/core';

import { AppService } from '../../services/app.service';
import { CookieService } from '../../services/cookie.service';
import { BaseComponent } from '../../base.component';

import { environment as globals } from '../../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BaseComponent implements OnInit {

  appName = globals.appName;
  isDarkTheme = false;
  recipientMenu = [
    {title: 'Wallet', link: ['wallet']},
  ];
  managerMenu = [
    {title: 'Recipient Management', link: ['manage', 'recipient']},
    {title: 'Badge Management', link: ['manage', 'badge']},
    // {title: 'Pathway Management', link: ['manage', 'pathway']}
  ];
  adminMenu = [
    {title: 'Organization Management', link: ['manage', 'organization']},
    {title: 'Issuer Management', link: ['manage', 'issuer']},
  ];

  constructor(private appService: AppService,
              private cookie: CookieService,
              protected injector: Injector) {
    super(injector);
    this.isDarkTheme = cookie.getCookieBool('darkTheme') || false;
  }

  ngOnInit(): void {
  }

  toggleTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
    this.appService.setDarkTheme(this.isDarkTheme);
  }
}
