import { Component, Inject, Injector, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MainComponent } from '../../../main.component';
import { Assertion } from '../../../ds/assertion';
import { Lang } from '../../../ds/lang';
import { Message } from '../../../ds/message';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss']
})
export class WalletComponent extends MainComponent implements OnInit {

  assertions: Assertion[];
  isLoading = false;

  constructor(public dialog: MatDialog,
              private bottomSheet: MatBottomSheet,
              protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.getAssertions();
  }

  getAssertions(): void {
    this.isLoading = true;
    this.api.assertionsGet()
      .subscribe(data => {
        this.isLoading = false;
        if (data.length === 0) {
          this.openSnackBar('No badges found!', 'Ok');
        }
        this.assertions = data;
      }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  openAssertionDialog(assertion: Assertion): void {
    this.dialog.open(AssertionDialogComponent, {width: '800px', data: assertion});
  }

  openShareBottomSheet(assertion: Assertion): void {
    this.bottomSheet.open(ShareBottomSheetComponent, {data: assertion});
  }
}

@Component({
  selector: 'app-dialog-assertion',
  templateUrl: './assertion.dialog.html',
  styleUrls: ['./assertion.dialog.scss', '../../../shared/forms.scss']
})
export class AssertionDialogComponent {

  languages: Lang[] = [
    {value: '[en]', viewValue: 'English'},
    {value: '[es]', viewValue: 'Spanish'},
    {value: '[hm]', viewValue: 'Hmong'}
  ];
  selectedLanguage = this.languages[0].value;

  constructor(
    public dialogRef: MatDialogRef<AssertionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public assertion: Assertion) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-bottom-sheet-share',
  templateUrl: './share.bottom-sheet.html'
})
export class ShareBottomSheetComponent extends MainComponent implements OnInit {

  baseUrl: string;

  constructor(
    protected injector: Injector,
    public dialog: MatDialog,
    private bottomSheetRef: MatBottomSheetRef<ShareBottomSheetComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public assertion: Assertion) {
    super(injector);
  }

  ngOnInit(): void {
    const parsedUrl = new URL(window.location.href);
    this.baseUrl = parsedUrl.origin;
  }

  action(event: MouseEvent, action: string): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
    switch (action) {
      case 'email':
        this.openDialog();
        break;
      case 'tweet':
        const text = `I just received my ${this.assertion.badge.name} badge! Check it out! ${this.baseUrl}/wallet/${this.assertion._id}`;
        window.open('https://twitter.com/intent/tweet?text=' + encodeURI(text), '_blank');
        break;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ShareEmailAddressComponent, {
      width: '280px',
      data: null
    });

    dialogRef.afterClosed().subscribe(email => {
      if (email) {
        this.sendEmail(email);
      }
    });
  }

  sendEmail(email): void {
    this.api.assertionEmail(this.assertion._id, {email})
      .subscribe((data: Message) => {
        this.openSnackBar(data.message, 'Ok', true);
      }, err => this.openSnackBar(err.message, 'Ok', false));
  }
}

@Component({
  selector: 'app-email-address-share',
  templateUrl: './share.email-address.html',
  styleUrls: ['./email-address.dialog.scss', '../../../shared/forms.scss']

})
export class ShareEmailAddressComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ShareEmailAddressComponent>,
    @Inject(MAT_DIALOG_DATA) public email: string) {
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
