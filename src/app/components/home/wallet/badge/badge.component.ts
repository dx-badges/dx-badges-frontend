import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MainComponent } from '../../../../main.component';
import { Assertion } from '../../../../ds/assertion';
import { Lang } from '../../../../ds/lang';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss', '../../../../shared/forms.scss']
})
export class BadgeComponent extends MainComponent implements OnInit {

  languages: Lang[] = [
    {value: '[en]', viewValue: 'English'},
    {value: '[es]', viewValue: 'Spanish'},
    {value: '[hm]', viewValue: 'Hmong'}
  ];
  selectedLanguage = this.languages[0].value;

  assertion: Assertion;

  constructor(protected injector: Injector,
              private route: ActivatedRoute) {
    super(injector);
  }

  ngOnInit(): void {
    const assertionId = this.route.snapshot.paramMap.get('id');
    this.getAssertion(assertionId);
  }

  getAssertion(assertionId: string): void {
    this.api.assertionGet(assertionId)
      .subscribe(data => {
        this.assertion = data;
      }, err => this.openSnackBar(err.message, 'Ok', false));
  }

}
