import { AfterViewInit, Component, ElementRef, Inject, Injector, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { debounceTime, filter, finalize, map, startWith, switchMap, tap } from 'rxjs/operators';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

import { MainComponent } from '../../../main.component';
import { Issuer } from '../../../ds/issuer';
import { Badge } from '../../../ds/badge';
import { LylesPerson } from '../../../ds/lyles_person';
import { Utils } from '../../../shared/utils';

import { environment as globals } from '../../../../environments/environment';

@Component({
  selector: 'app-badge-mgt',
  templateUrl: './badge-mgt.component.html',
  styleUrls: ['./badge-mgt.component.scss', '../../../shared/forms.scss', '../../../shared/table.scss']
})
export class BadgeMgtComponent extends MainComponent implements OnInit {
  displayedCSVColumns: string[] = ['name', 'email', 'schoolName', 'gradeLevel', 'account'];
  displayedBadgesColumns: string[] = ['image', 'name', '#', 'criteria'];

  createForm: FormGroup;
  issueForm: FormGroup;
  issuers: Issuer[] = [];
  badges: Badge[] = [];
  selectedIssuerId;
  imageCSrc: any;
  minDate: Date;
  filteredRecipients: LylesPerson[];
  issueRecipients: LylesPerson[];
  isRLoading = false;

  constructor(private formBuilder: FormBuilder,
              public dialog: MatDialog,
              protected injector: Injector) {
    super(injector);
    this.minDate = new Date();
    this.selectedIssuerId = undefined;
  }

  ngOnInit(): void {
    this.createForms();
    this.getIssuersByUser();

    this.i_recipient.valueChanges
      .pipe(
        startWith(''),
        map(value => (typeof value === 'string' ? value : value.name)),
        filter(query => query?.length > 2),
        debounceTime(700),
        tap(() => {
          this.filteredRecipients = [];
          this.isRLoading = true;
        }),
        switchMap(value => this.api.recipientSearch({search: value})
          .pipe(
            finalize(() => {
              this.isRLoading = false;
            })
          )
        )
      )
      .subscribe(data => {
        if (data.result.length === 0) {
          this.filteredRecipients = [];
          this.openSnackBar('No matching results found', 'Ok');
        } else {
          this.filteredRecipients = data.result;
        }
      }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  getIssuersByUser(): void {
    this.api.issuersByUserGet()
      .subscribe(
        (data: Issuer[]) => {
          if (data.length === 0) {
            this.openTimedSnackBar('No programs was assigned to you', 'Ok', 6000);
          }
          this.issuers = data;
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  getBadgesByIssuer(): void {
    this.api.badgesByIssuerGet(this.selectedIssuerId)
      .subscribe(
        (data: Badge[]) => {
          this.badges = data;
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  // ----------------------- CREATE ------------------------------ //

  get c_name(): AbstractControl {
    return this.createForm.get('name');
  }

  get c_criteria(): AbstractControl {
    return this.createForm.get('criteria');
  }

  get c_alignment(): AbstractControl {
    return this.createForm.get('alignment');
  }

  get c_tags(): AbstractControl {
    return this.createForm.get('tags');
  }

  get c_expirationDate(): AbstractControl {
    return this.createForm.get('expirationDate');
  }

  get c_description(): AbstractControl {
    return this.createForm.get('description');
  }

  getCErrorMessage(field: string): string {
    switch (field) {
      case 'name':
        if (this.c_name.hasError('required')) {
          return 'Name is <strong>required</strong>';
        }
        if (this.c_name.hasError('minlength')) {
          return `Name must be <strong>at least ${this.c_name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.c_name.hasError('maxlength')) {
          return `Name must be <strong>less than ${this.c_name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> name';
    }
  }

  onCreate(formData: FormGroup, formDirective: FormGroupDirective): void {
    if (this.selectedIssuerId) {
      this.api.badgeCreate(
        this.imageCSrc ?
          {...formData.value, ...{issuer: this.selectedIssuerId}, ...{image: this.imageCSrc}} :
          {...formData.value, ...{issuer: this.selectedIssuerId}})
        .subscribe(
          (data) => {
            formDirective.resetForm();
            this.createForm.reset();
            this.imageCSrc = undefined;
            this.getBadgesByIssuer();
            this.openSnackBar(data.message, 'Ok', true);
          }, err => this.openSnackBar(err.message, 'Ok', false));
    } else {
      this.openSnackBar('Please select a program', 'Ok', false);
    }
  }

  onCFileChange($event): void {
    if (($event.target as HTMLInputElement).files && ($event.target as HTMLInputElement).files[0]) {
      if (($event.target as HTMLInputElement).files[0].size < 1024 * 1024 * 5) { // 5MB
        Utils.resizeImage({file: ($event.target as HTMLInputElement).files[0], maxSize: globals.badgeMaxSize})
          .then(img => this.imageCSrc = img)
          .catch(err => this.openSnackBar(err.message, 'Ok', false));
      } else {
        this.openSnackBar('Image file is too large.', 'Ok', false);
      }
    } else {
      this.imageCSrc = undefined;
    }
  }

  // ----------------------- ISSUE ----------------------------- //

  get i_recipient(): AbstractControl {
    return this.issueForm.get('recipient');
  }

  get i_badge(): AbstractControl {
    return this.issueForm.get('badge');
  }

  get i_issuedOn(): AbstractControl {
    return this.issueForm.get('issuedOn');
  }

  get i_expires(): AbstractControl {
    return this.issueForm.get('expires');
  }

  get i_evidence(): AbstractControl {
    return this.issueForm.get('evidence');
  }

  get i_narrative(): AbstractControl {
    return this.issueForm.get('narrative');
  }

  getIErrorMessage(field: string): string {
    switch (field) {
      case 'recipient':
        if (this.c_name.hasError('required')) {
          return 'Recipient is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> recipient';
      case 'badge':
        if (this.c_name.hasError('required')) {
          return 'Badge is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> badge';
    }
  }

  getRecipient(formData: FormGroup): string | string[] {
    if (formData.value.recipient && typeof formData.value.recipient === 'string') {
      console.log('str');
      return formData.value.recipient;
    } else if (formData.value.recipient && typeof formData.value.recipient === 'object') {
      console.log('obj');
      return formData.value.recipient._id;
    } else if (this.issueRecipients && this.issueRecipients.length > 0) {
      console.log('arr');
      return this.issueRecipients.map(r => r._id);
    } else {
      console.log('null');
      return null;
    }
  }

  onIssue(formData: FormGroup, formDirective: FormGroupDirective): void {
    const assertion = {
      recipient: this.getRecipient(formData),
      badge: formData.value.badge,
      expires: formData.value.expires,
      evidence: formData.value.evidence,
      narrative: formData.value.narrative
    };
    if (assertion.recipient) {
      this.api.badgeIssueToRecipient(assertion.badge, assertion)
        .subscribe(data => {
          formDirective.resetForm();
          this.issueForm.reset();
          this.openTimedSnackBar(data.message, 'Ok', 6000, true);
        }, err => this.openSnackBar(err.message, 'Ok', false));
    } else {
      this.openSnackBar('Please select recipients', 'Ok', true);
    }
  }

  // ----------------------- FORMS ------------------------------ //

  private createForms(): void {
    this.createForm = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(64)
      ])],
      criteria: [''],
      alignment: [''],
      tags: [''],
      expirationDate: [''],
      image: [''],
      description: ['']
    });

    this.issueForm = this.formBuilder.group({
      recipient: [''],
      badge: ['', Validators.required],
      issuedOn: ['Today'],
      expires: [''],
      evidence: [''],
      narrative: ['']
    });
  }

  // ----------------------- OTHERS ------------------------------ //

  onProgSelect(event): void {
    if (event.isUserInput) {
      if (event.source.value) {
        this.selectedIssuerId = event.source.value;
        this.getBadgesByIssuer();
      } else {
        this.selectedIssuerId = undefined;
      }
    }
  }

  onBadgeSelect(event): void {
    if (event.isUserInput) {
      if (event.source.value) {
        // this.selectedBadgeId = event.source.value;
        // console.log(event.source.value);
      } else {
        // this.selectedBadgeId = undefined;
      }
    }
  }

  onBadgeClick(): void {
    if (!this.selectedIssuerId) {
      this.openSnackBar('Please select a program first', 'Ok', false);
    } else if (this.badges.length === 0) {
      this.openSnackBar('This program has no badge', 'Ok', false);
    }
  }

  displayFn(recipient: LylesPerson): string {
    if (recipient) {
      return recipient.name;
    }
  }

  openDialogUpload(): void {
    const dialogRef = this.dialog.open(UploadCSVDialogComponent, {
      width: '600px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(recipients => {
      if (recipients && recipients.length > 0) {
        this.onBulkRecipientCreate(recipients);
      }
    });
  }

  onBulkRecipientCreate(recipients: LylesPerson[]): void {
    this.api.recipientCreate(recipients)
      .subscribe(
        (data) => {
          if (Array.isArray(data.recipient)) {
            this.issueRecipients = data.recipient;
          }
          this.openSnackBar(data.message, 'Ok', true);
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }
}

export interface UploadCSVDialogData {
  recipients: LylesPerson[];
}

@Component({
  selector: 'app-dialog-upload-csv',
  templateUrl: 'upload-csv.dialog.html',
  styleUrls: ['../../../shared/table.scss']

})

export class UploadCSVDialogComponent implements AfterViewInit {
  errMsg: string;
  @ViewChild('file') fileInput: ElementRef;

  displayedColumns: string[] = ['name', 'email', 'schoolName', 'gradeLevel'];

  constructor(
    public dialogRef: MatDialogRef<UploadCSVDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UploadCSVDialogData) {
    this.errMsg = '';
  }

  ngAfterViewInit(): void {
    (this.fileInput.nativeElement as HTMLElement).click();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFileChange($event): void {
    this.errMsg = '';
    this.data.recipients = [];
    if (($event.target as HTMLInputElement).files && ($event.target as HTMLInputElement).files[0]) {
      const file = ($event.target as HTMLInputElement).files[0];
      if (file.size < 1024 * 1024 * 5) { // 5MB
        if (this.isValidCSVFile(file)) {

          const reader = new FileReader();
          reader.readAsText(file);
          reader.onload = () => {
            const csvData = reader.result;
            const csvRecordsArray = (csvData as string).split(/\r\n|\n/);
            // const headersRow = this.getHeaderArray(csvRecordsArray);
            const headersRow = ['Student Name', 'Student Last Name', 'Student Email', 'School Name', 'Grade Level'];

            for (let i = 1; i < csvRecordsArray.length; i++) {
              if (!csvRecordsArray[i].startsWith('**')) {
                const currentRecord = (csvRecordsArray[i] as string).split(',');
                if (currentRecord.length >= 5) {
                  const person: LylesPerson = {
                    // Last name, First name
                    name: currentRecord[2].replace(/"/g, '').trim() + ', ' +
                      currentRecord[1].replace(/"/g, '').trim(),
                    email: currentRecord[3].replace(/"/g, '').trim(),
                    schoolName: currentRecord[5].replace(/"/g, '').trim(),
                    gradeLevel: currentRecord[4].replace(/"/g, '').trim(),
                  };
                  if (person.name && person.email) {
                    this.data.recipients.push(person);
                  }
                }
              }
            }
            // TODO keep the last one
            this.data.recipients = this.data.recipients.filter((v, i, a) => a.findIndex(t => (t.email === v.email)) === i);
          };

        } else {
          this.errMsg = 'Please import valid .csv file.';
        }
      } else {
        this.errMsg = 'CSV file is too large.';
      }
    }
  }

  isValidCSVFile(file: any): boolean {
    return file.name.endsWith('.csv');
  }

  getHeaderArray(csvRecordsArr: any): string[] {
    const headers = (csvRecordsArr[0] as string).split(',');
    const headerArray = [];
    for (const header of headers) {
      headerArray.push(header);
    }
    return headerArray;
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any): string[] {
    const csvArr = [];
    for (let i = 1; i < csvRecordsArray.length; i++) {
      const currentRecord = (csvRecordsArray[i] as string).split(',');
      /*csvRecord.id = currentRecord[0].trim();
      csvRecord.firstName = currentRecord[1].trim();
      csvRecord.lastName = currentRecord[2].trim();
      csvRecord.age = currentRecord[3].trim();
      csvRecord.position = currentRecord[4].trim();
      csvRecord.mobile = currentRecord[5].trim();
      csvArr.push(csvRecord);*/
    }
    return csvArr;
  }
}
