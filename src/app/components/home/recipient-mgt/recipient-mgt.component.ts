import { AfterViewInit, Component, Inject, Injector, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { merge } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

import { MainComponent } from '../../../main.component';
import { Message } from '../../../ds/message';
import { LylesPerson } from '../../../ds/lyles_person';

import { environment as globals } from '../../../../environments/environment';
import { Utils } from '../../../shared/utils';
import { Issuer } from '../../../ds/issuer';

@Component({
  selector: 'app-recipient-mgt',
  templateUrl: './recipient-mgt.component.html',
  styleUrls: ['./recipient-mgt.component.scss', '../../../shared/forms.scss', '../../../shared/table.scss', '../wallet/wallet.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class RecipientMgtComponent extends MainComponent implements OnInit, AfterViewInit {

  // SEARCH
  searchForm: FormGroup;
  displayedColumns: string[] = ['image', 'name', 'email', 'schoolName', 'gradeLevel', 'issued_badges', 'actions'];
  searchRecipients: LylesPerson[] = [];
  expandedElement: LylesPerson | null;
  searchResultPerPage = 15;
  searchResultTotalCount = 0;
  searchIsLoadingResult = true;
  @ViewChild(MatPaginator) searchPaginator: MatPaginator;
  @ViewChild(MatSort) searchSort: MatSort;

  // OTHERS
  createForm: FormGroup;
  avatarCSrc: any;

  constructor(private formBuilder: FormBuilder,
              public dialog: MatDialog,
              protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.createForms();
  }

  ngAfterViewInit(): void {
    this.searchSort.sortChange.subscribe(() => this.searchPaginator.pageIndex = 0);

    merge(this.searchSort.sortChange, this.searchPaginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {
          this.searchIsLoadingResult = true;
          const query = {
            page: this.searchPaginator.pageIndex,
            limit: this.searchResultPerPage,
            sort: this.searchSort.active,
            direction: this.searchSort.direction,
            count: true
          };
          return this.api.recipientSearch({...this.searchForm.value, ...query});
        }),
        map(data => {
          this.searchIsLoadingResult = false;
          return data;
        }))
      .subscribe(data => {
        this.searchResultTotalCount = data.count;
        if (data.result.length === 0) {
          this.openSnackBar('No matching results found', 'Ok');
        }
        this.searchRecipients = data.result;
        console.log(this.searchRecipients);
      }, err => {
        this.searchIsLoadingResult = false;
        this.openSnackBar(err.message, 'Ok', false);
      });
  }

  get search(): AbstractControl {
    return this.searchForm.get('search');
  }

  onSearch(): void {
    console.log(this.searchForm.value);
    this.searchPaginator.pageIndex = 0;
    this.searchIsLoadingResult = true;
    const query = {
      page: this.searchPaginator.pageIndex,
      limit: this.searchResultPerPage,
      sort: this.searchSort.active,
      direction: this.searchSort.direction,
      count: true
    };
    this.api.recipientSearch({...this.searchForm.value, ...query})
      .subscribe((data) => {
        this.searchIsLoadingResult = false;
        this.searchResultTotalCount = data.count;
        if (data.result.length === 0) {
          this.openSnackBar('No matching results found', 'Ok');
        }
        this.searchRecipients = data.result;
      }, err => {
        this.searchIsLoadingResult = false;
        this.openSnackBar(err.message, 'Ok', false);
      });
  }

  openAssignIssuerDialog(recipient: LylesPerson): void {
    this.api.issuersGet()
      .subscribe(issuers => {
        if (issuers && issuers.length > 0) {
          const dialogRef = this.dialog.open(AssignIssuerDialogComponent, {
            width: '350px',
            data: {recipient, issuers, issuer: null}
          });
          dialogRef.afterClosed().subscribe(result => {
            if (result) {
              this.api.recipientIssuer(recipient._id, result)
                .subscribe(data => {
                  this.openSnackBar(data.message, 'Ok', true);
                }, err => {
                  this.openSnackBar(err.message, 'Ok', false);
                });
            }
          });
        } else {
          this.openSnackBar('No issuers found', 'Ok');
        }
      }, err => {
        this.openSnackBar(err.message, 'Ok', false);
      });
  }

  openRecipientDetailsDialog(recipient: LylesPerson): void {
    const dialogRef = this.dialog.open(RecipientDetailsDialogComponent, {
      width: '600px',
      data: {recipient}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.api.recipientUpdate(result.recipient._id, {...result.recipient, ...{image: result.avatarSrc}})
          .subscribe(data => {
            this.openSnackBar(data.message, 'Ok', true);
            this.onSearch();
          }, err => this.openSnackBar(err.message, 'Ok', false));
      }
    });
  }

  expandTable(recipient: LylesPerson): void {
    console.log('EXPAND!');
  }

  // ------------------------- CREATE ------------------------- //

  get c_first_name(): AbstractControl {
    return this.createForm.get('firstName');
  }

  get c_last_name(): AbstractControl {
    return this.createForm.get('lastName');
  }

  get c_email(): AbstractControl {
    return this.createForm.get('email');
  }

  get c_image(): AbstractControl {
    return this.createForm.get('image');
  }

  get c_telephone(): AbstractControl {
    return this.createForm.get('telephone');
  }

  get c_description(): AbstractControl {
    return this.createForm.get('description');
  }

  getCErrorMessage(field: string): string {
    switch (field) {
      case 'firstName':
        if (this.c_first_name.hasError('required')) {
          return 'First name is <strong>required</strong>';
        }
        if (this.c_first_name.hasError('minlength')) {
          return `First name must be <strong>at least ${this.c_first_name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.c_first_name.hasError('maxlength')) {
          return `First name must be <strong>less than ${this.c_first_name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> first name';
      case 'lastName':
        if (this.c_last_name.hasError('required')) {
          return 'Last name is <strong>required</strong>';
        }
        if (this.c_last_name.hasError('minlength')) {
          return `Last name must be <strong>at least ${this.c_last_name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.c_last_name.hasError('maxlength')) {
          return `Last name must be <strong>less than ${this.c_last_name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> last name';
      case 'email':
        if (this.c_email.hasError('required')) {
          return 'Email is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> email address';
      case 'telephone':
        return '<strong>Invalid</strong> telephone';
    }
  }

  onCreate(formData: FormGroup, formDirective: FormGroupDirective): void {

    const rec = this.avatarCSrc ? {...formData.value, ...{image: this.avatarCSrc}} : formData.value;
    rec.name = formData.get('lastName').value + ', ' + formData.get('firstName').value;

    this.api.recipientCreate(rec)
      .subscribe(
        (data: Message) => {
          formDirective.resetForm();
          this.createForm.reset();
          this.createForm.get('type').setValue('Profile');
          this.avatarCSrc = undefined;
          this.openSnackBar(data.message, 'Ok', true);
          this.onSearch();
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  onCFileChange($event): void {
    if (($event.target as HTMLInputElement).files && ($event.target as HTMLInputElement).files[0]) {
      if (($event.target as HTMLInputElement).files[0].size < 1024 * 1024 * 5) { // 5MB
        Utils.resizeImage({file: ($event.target as HTMLInputElement).files[0], maxSize: globals.avatarMinSize})
          .then(img => this.avatarCSrc = img)
          .catch(err => this.openSnackBar(err.message, 'Ok', false));
      } else {
        this.openSnackBar('Image file is too large.', 'Ok', false);
      }
    } else {
      this.avatarCSrc = null;
    }
  }

  // ----------------------- FORMS ------------------------------ //

  private createForms(): void {
    this.searchForm = this.formBuilder.group({
      search: ['', Validators.maxLength(64)],
    });

    this.createForm = this.formBuilder.group({
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.pattern(/^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/)
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.pattern(/^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/)
      ])],
      type: ['Profile'],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      image: [''],
      telephone: ['', Validators.pattern(/[0-9]{10}/)],
      description: [''],
      schoolName: [''],
      gradeLevel: ['']
    });
  }
}

export interface AssignIssuerData {
  recipient: LylesPerson;
  issuers: Issuer[];
  issuer: Issuer;
}

export interface RecipientDetailsData {
  recipient: LylesPerson;
}

@Component({
  selector: 'app-dialog-assign-issuer',
  templateUrl: 'assign-issuer.dialog.html',
  styleUrls: ['assign-issuer.dialog.scss']
})
export class AssignIssuerDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<AssignIssuerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AssignIssuerData) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'app-dialog-recipient-details',
  templateUrl: 'recipient-details.dialog.html',
  styleUrls: ['recipient-details.dialog.scss', '../../../shared/forms.scss']
})
export class RecipientDetailsDialogComponent extends MainComponent implements OnInit {

  updateForm: FormGroup;
  avatarSrc: any;

  constructor(
    private formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<RecipientDetailsDialogComponent>,
    protected injector: Injector,
    @Inject(MAT_DIALOG_DATA) public data: RecipientDetailsData) {
    super(injector);
  }

  ngOnInit(): void {
    this.createForms();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get name(): AbstractControl {
    return this.updateForm.get('name');
  }

  get email(): AbstractControl {
    return this.updateForm.get('email');
  }

  get image(): AbstractControl {
    return this.updateForm.get('image');
  }

  get telephone(): AbstractControl {
    return this.updateForm.get('telephone');
  }

  get description(): AbstractControl {
    return this.updateForm.get('description');
  }

  private createForms(): void {
    this.updateForm = this.formBuilder.group({
      _id: [this.data.recipient._id],
      name: [this.data.recipient.name, Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(64),
        Validators.pattern(/^([a-zA-Z]+?),?([-\s'][a-zA-Z]+)*?$/)
      ])],
      type: [this.data.recipient.type],
      email: [this.data.recipient.email, Validators.compose([
        Validators.required,
        Validators.email
      ])],
      image: [''],
      telephone: [this.data.recipient.telephone, Validators.pattern(/[0-9]{10}/)],
      description: [this.data.recipient.description],
      schoolName: [this.data.recipient.schoolName],
      gradeLevel: [this.data.recipient.gradeLevel]
    });
    this.avatarSrc = this.data.recipient.image;
  }

  getErrorMessage(field: string): string {
    switch (field) {
      case 'name':
        if (this.name.hasError('required')) {
          return 'Name is <strong>required</strong>';
        }
        if (this.name.hasError('minlength')) {
          return `Name must be <strong>at least ${this.name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.name.hasError('maxlength')) {
          return `Name must be <strong>less than ${this.name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> name';
      case 'email':
        if (this.email.hasError('required')) {
          return 'Email is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> email address';
      case 'telephone':
        return '<strong>Invalid</strong> telephone';
    }
  }

  onFileChange($event): void {
    if (($event.target as HTMLInputElement).files && ($event.target as HTMLInputElement).files[0]) {
      if (($event.target as HTMLInputElement).files[0].size < 1024 * 1024 * 5) { // 5MB
        Utils.resizeImage({file: ($event.target as HTMLInputElement).files[0], maxSize: globals.avatarMinSize})
          .then(img => this.avatarSrc = img)
          .catch(err => this.openSnackBar(err.message, 'Ok', false));
      } else {
        this.openSnackBar('Image file is too large.', 'Ok', false);
      }
    } else {
      this.avatarSrc = null;
    }
  }
}
