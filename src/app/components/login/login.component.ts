import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { MainComponent } from '../../main.component';
import { Login } from '../../ds/login';
import { environment as globals } from '../../../environments/environment';
import { Ping } from '../../ds/ping';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../../shared/forms.scss']
})
export class LoginComponent extends MainComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  redirectUrl: string;
  authOrigin: string;
  appName = globals.appName;

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.makeLog('Login: ngOnInit');
    this.route.queryParams.subscribe(params => {
      this.redirectUrl = params?.redirect;
      this.authOrigin = params?.auth_origin;
    });
    this.createForm();
  }

  ngAfterViewInit(): void {
    if (this.authOrigin) {
      this.api.ping(this.authOrigin)
        .subscribe(data => {
          this.openDelayedSnackBar('Welcome back!', 'Ok', 500, true);
          this.router.navigate([this.redirectUrl ? this.redirectUrl : '/'], {replaceUrl: true}).catch(console.error);
        }, err => this.openSnackBar(err.message, 'Ok', false));
    }
  }

  login(): void {
    this.api.login(this.loginForm.value)
      .subscribe(
        (data: Login) => {
          this.openDelayedSnackBar(data.message, 'Ok', 500, true);
          // TODO - message "close this tab"
          // this.router.navigate([this.redirectUrl ? this.redirectUrl : '/'], {replaceUrl: true}).catch(console.error);
        }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  get email(): AbstractControl {
    return this.loginForm.get('email');
  }

  private createForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email,
      ])],
      remember: [true, Validators.required],
    });
  }

  getErrorMessage(field: string): string {
    switch (field) {
      case 'email':
        if (this.email.hasError('required')) {
          return 'email is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> email address';
    }
  }
}
