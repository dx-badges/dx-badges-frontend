import { Component, Injector, OnInit } from '@angular/core';
import { MainComponent } from '../../main.component';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent extends MainComponent implements OnInit {

  constructor(protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.onLogout();
  }

  private onLogout(): void {
    this.api.logoutAndNotify();
    this.openDelayedSnackBar('Logged out successfully.', 'Ok');
    this.router.navigate(['/login']).catch(console.error);
  }
}
