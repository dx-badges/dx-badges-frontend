import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { MainComponent } from '../../main.component';
import { ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment as globals } from '../../../environments/environment';
import { Message } from '../../ds/message';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../login/login.component.scss', './register.component.scss', '../../shared/forms.scss']
})
export class RegisterComponent extends MainComponent implements OnInit, AfterViewInit {

  registerForm: FormGroup;
  appName = globals.appName;

  constructor(private route: ActivatedRoute,
              private formBuilder: FormBuilder,
              protected injector: Injector) {
    super(injector);
  }

  ngOnInit(): void {
    this.makeLog('Register: ngOnInit');
    this.createForm();
  }

  ngAfterViewInit(): void {
  }

  register(): void {

    this.api.register({
      ...this.registerForm.value,
      ...{name: this.registerForm.get('lastName').value + ', ' + this.registerForm.get('firstName').value}
    }).subscribe((data: Message) => {
      this.openSnackBar(data.message, 'Ok', true);
    }, err => this.openSnackBar(err.message, 'Ok', false));
  }

  get first_name(): AbstractControl {
    return this.registerForm.get('firstName');
  }

  get last_name(): AbstractControl {
    return this.registerForm.get('lastName');
  }

  get email(): AbstractControl {
    return this.registerForm.get('email');
  }

  get image(): AbstractControl {
    return this.registerForm.get('image');
  }

  get telephone(): AbstractControl {
    return this.registerForm.get('telephone');
  }

  private createForm(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.pattern(/^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/)
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(32),
        Validators.pattern(/^([a-zA-Z]+?)([-\s'][a-zA-Z]+)*?$/)
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      image: [''],
      telephone: ['', Validators.pattern(/[0-9]{10}/)],
    });
  }

  getErrorMessage(field: string): string {
    switch (field) {
      case 'firstName':
        if (this.first_name.hasError('required')) {
          return 'First name is <strong>required</strong>';
        }
        if (this.first_name.hasError('minlength')) {
          return `First name must be <strong>at least ${this.first_name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.first_name.hasError('maxlength')) {
          return `First name must be <strong>less than ${this.first_name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> first name';
      case 'lastName':
        if (this.last_name.hasError('required')) {
          return 'Last name is <strong>required</strong>';
        }
        if (this.last_name.hasError('minlength')) {
          return `Last name must be <strong>at least ${this.last_name.getError('minlength').requiredLength}</strong> characters`;
        }
        if (this.last_name.hasError('maxlength')) {
          return `Last name must be <strong>less than ${this.last_name.getError('maxlength').requiredLength}</strong> characters`;
        }
        return '<strong>Invalid</strong> last name';
      case 'email':
        if (this.email.hasError('required')) {
          return 'Email is <strong>required</strong>';
        }
        return '<strong>Invalid</strong> email address';
      case 'telephone':
        return '<strong>Invalid</strong> telephone';
    }
  }
}
