import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IsNotAuthGuard } from './guards/is-not-auth.guard';
import { IsAuthGuard } from './guards/is-auth.guard';
import { PermissionGuard } from './guards/permission.guard';

import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

import { WalletComponent } from './components/home/wallet/wallet.component';
import { RecipientMgtComponent } from './components/home/recipient-mgt/recipient-mgt.component';
import { BadgeMgtComponent } from './components/home/badge-mgt/badge-mgt.component';
import { OrganizationMgtComponent } from './components/home/organization-mgt/organization-mgt.component';
import { IssuerMgtComponent } from './components/home/issuer-mgt/issuer-mgt.component';
import { BadgeComponent } from './components/home/wallet/badge/badge.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent, canActivate: [IsNotAuthGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [IsAuthGuard]},
  {path: 'register', component: RegisterComponent, canActivate: [IsNotAuthGuard]},
  {
    path: '',
    component: HomeComponent,
    children: [
      {path: '', redirectTo: 'wallet', pathMatch: 'full'},
      {path: 'wallet', component: WalletComponent, canActivate: [IsAuthGuard]},
      {path: 'wallet/:id', component: BadgeComponent},
      {path: 'manage', redirectTo: 'manage/recipient', pathMatch: 'full'},
      {
        path: 'manage/recipient',
        component: RecipientMgtComponent,
        canActivate: [PermissionGuard],
        data: {roles: ['Admin', 'Manager'], redirectTo: ['/']}
      },
      {
        path: 'manage/badge',
        component: BadgeMgtComponent,
        canActivate: [PermissionGuard],
        data: {roles: ['Admin', 'Manager'], redirectTo: ['/']}
      },
      {
        path: 'manage/organization',
        component: OrganizationMgtComponent,
        canActivate: [PermissionGuard],
        data: {roles: ['Admin'], redirectTo: ['/']}
      },
      {
        path: 'manage/issuer',
        component: IssuerMgtComponent,
        canActivate: [PermissionGuard],
        data: {roles: ['Admin'], redirectTo: ['/']}
      },
    ]
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
