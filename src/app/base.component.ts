import { Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { CommunicationService } from './services/communication.service';
import { ApiService } from './services/api.service';
import { Person } from './ds/person';
import { environment as globals } from '../environments/environment';

export abstract class BaseComponent {
  private API: ApiService;
  private COMMUNICATION: CommunicationService;
  private ROUTER: Router;

  public loggedIn;
  public user: Person;

  protected constructor(protected injector: Injector) {
    this.loggedIn = this.api.loggedIn;
    if (this.loggedIn) {
      this.getUser();
    }
    this.communication.updateUser.subscribe(() => this.getUser());
  }

  private getUser(): void {
    this.api.me.subscribe(res => this.user = res.user, (err: HttpErrorResponse) => console.error(err.message));
  }

  public makeLog(message: string, error = false): void {
    if (!globals.production) {
      if (error) {
        console.error(message);
      } else {
        console.log(message);
      }
    }
  }

  get isAdmin(): boolean {
    return this.user && this.user.type === 'Admin';
  }

  get isManager(): boolean {
    return this.user && ['Admin', 'Manager'].indexOf(this.user.type) >= 0;
  }

  protected get api(): ApiService {
    if (!this.API) {
      this.API = this.injector.get(ApiService);
    }
    return this.API;
  }

  protected get router(): Router {
    if (!this.ROUTER) {
      this.ROUTER = this.injector.get(Router);
    }
    return this.ROUTER;
  }

  protected get communication(): CommunicationService {
    if (!this.COMMUNICATION) {
      this.COMMUNICATION = this.injector.get(CommunicationService);
    }
    return this.COMMUNICATION;
  }

  public authAndNavigate(link: string[]): void {
    if (this.loggedIn) {
      this.router.navigate(link).catch(console.error);
    } else {
      this.router.navigate(['/login'], {queryParams: {redirect: link.toString()}}).catch(console.error);
    }
  }
}
