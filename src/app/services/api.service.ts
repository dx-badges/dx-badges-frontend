import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, shareReplay, tap } from 'rxjs/operators';

import { CommunicationService } from './communication.service';
import { Login } from '../ds/login';
import { Ping } from '../ds/ping';
import { Person } from '../ds/person';
import { Message } from '../ds/message';
import { Organization } from '../ds/organization';
import { Issuer } from '../ds/issuer';
import { Badge } from '../ds/badge';
import { Assertion } from '../ds/assertion';
import { LylesPerson } from '../ds/lyles_person';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient,
              private router: Router,
              private communication: CommunicationService) {
  }

  public me$: Observable<Login | Ping>;

  get me(): Observable<Ping> {
    // console.log('get: me');
    if (!this.me$) {
      this.me$ = this.ping()
        .pipe(
          tap(ApiService.setSession),
          shareReplay(1)
        );
      return this.me$;
    }
    return this.me$;
  }

  get loggedIn(): boolean {
    // console.log('get: loggedIn');
    return !!this.token;
  }

  get loggedOut(): boolean {
    // console.log('get: loggedOut');
    return !this.loggedIn;
  }

  get token(): string {
    // console.log('get: token');
    const expireAt = localStorage.getItem('userExpires');
    const token = localStorage.getItem('userToken');
    if (expireAt && token) {
      const expiresAt = JSON.parse(expireAt);
      // tslint:disable-next-line:no-bitwise
      if (((+new Date() / 1000) | 0) < expiresAt && token) {
        return token;
      } else {
        this.logout();
      }
    }
    return '';
  }

  private static setSession(res): void {
    if (res.token) {
      // console.log('API: setSession');
      // tslint:disable-next-line:no-bitwise
      const expireAt = ((+new Date() / 1000) | 0) + 60 * 60 * 24; // 1Day TODO
      localStorage.setItem('userToken', res.token);
      localStorage.setItem('userExpires', JSON.stringify(expireAt.valueOf()));
    }
  }

  // ** --------------- API Calls ------------------ ** //

  public ping(token: string = null): Observable<Ping> {
    console.log('GET: ping');
    return this.http.get<Ping>('api/person/me',
      {headers: this.getAuth(true, token), withCredentials: true})
      .pipe(
        tap(ApiService.setSession),
        shareReplay(1),
        catchError(this.handleError.bind(this))
      );
  }

  public login(user: {email: any}): Observable<Login | Ping> {
    console.log('POST: login');
    this.me$ = this.http.post<Login | Ping>('api/person/login', user,
      {headers: new HttpHeaders({'Content-Type': 'application/json'})})
      .pipe(
        tap(ApiService.setSession),
        shareReplay(1),
        catchError(this.handleError.bind(this))
      );
    return this.me$;
  }

  public logout(): void {
    console.log('API: logout');
    this.me$ = null;
    localStorage.removeItem('userToken');
    localStorage.removeItem('userExpires');
  }

  public logoutAndNotify(): void {
    console.log('API: logoutAndNotify');
    this.logout();
    this.communication.userLoggedOut(true);
    // TODO
    this.router.navigate(['/login'], {queryParams: {redirect: window.location.pathname}}).catch(console.error);
  }

  public register(user: any): Observable<Message> {
    console.log('POST: register');
    return this.http.post<Message>('api/person/register', user)
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** ---------------- Recipient ----------------** //

  public recipientSearch(query): Observable<{result: Person[] | LylesPerson[], count: number}> {
    console.log('GET: recipientSearch:', query);
    return this.http.get<{result: Person[] | LylesPerson[], count: number}>('api/recipients',
      {params: query, headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public recipientCreate(recipient: any): Observable<{recipient: Person | Person[], message: string}> {
    console.log('POST: recipientCreate');
    return this.http.post<{recipient: Person | Person[], message: string}>('api/recipients', recipient,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public recipientUpdate(recipientId: string, recipient: any): Observable<{recipient: Person, message: string}> {
    console.log('PATCH: recipientUpdate');
    console.log(recipient);
    return this.http.patch<{recipient: Person, message: string}>(`api/recipients/${recipientId}`, recipient,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public recipientIssuer(recipientId: string, issuerId: string): Observable<{recipient: Person, message: string}> {
    console.log('POST: recipientIssuer');
    return this.http.post<{recipient: Person, message: string}>(`api/recipients/${recipientId}/issuers`, {issuer: issuerId},
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** ---------------- Organization ----------------** //

  public organizationGet(): Observable<Organization[]> {
    console.log('GET: organizationsGet:');
    return this.http.get<Organization[]>('api/organizations',
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public organizationCreate(organization: any): Observable<{organization: Organization, message: string}> {
    console.log('POST: organizationsCreate');
    return this.http.post<{organization: Organization, message: string}>('api/organizations', organization,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** ------------------ Issuers -----------------** //

  public issuersGet(): Observable<Issuer[]> {
    console.log('GET: issuersGet:');
    return this.http.get<Issuer[]>(`api/issuers`,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public issuersByOrganizationGet(organization: string): Observable<Issuer[]> {
    console.log('GET: issuersByOrganizationGet:');
    return this.http.get<Issuer[]>(`api/organizations/${organization}/issuers`,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public issuerCreate(issuer: any): Observable<{issuer: Organization, message: string}> {
    console.log('POST: issuerCreate');
    return this.http.post<{organization: Organization, message: string}>('api/issuers', issuer,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public issuersByUserGet(): Observable<Issuer[]> {
    console.log('GET: issuersByUserGet:');
    return this.http.get<Issuer[]>('api/person/issuers',
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** ----------------- Badge ------------------** //

  public badgeCreate(badge: any): Observable<{issuer: Organization, message: string}> {
    console.log('POST: issuerCreate');
    return this.http.post<{badge: Badge, message: string}>('api/badges', badge,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public badgesByIssuerGet(issuerId: string): Observable<Badge[]> {
    console.log('GET: badgesByIssuerGet:');
    return this.http.get<Badge[]>(`api/issuers/${issuerId}/badges`,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public badgeIssueToRecipient(badgeId: string, assertion: any): Observable<Message> {
    console.log('POST: issuerCreate');
    return this.http.post<Message>(`api/badges/${badgeId}/issue`, assertion,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** --------- Assertions (Badges) -------------** //

  public assertionGet(assertionId: string): Observable<Assertion> {
    console.log('GET: assertionGet');
    return this.http.get<Assertion>(`api/assertions/${assertionId}`,
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public assertionsGet(): Observable<Assertion[]> {
    console.log('GET: assertionsGet');
    return this.http.get<Assertion[]>('api/assertions',
      {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  public assertionEmail(assertionId: string, email: {email}): Observable<Message> {
    console.log('GET: assertionGet');
    return this.http.post<Message>(`api/assertions/${assertionId}/email`,
      email, {headers: this.getAuth(), withCredentials: true})
      .pipe(catchError(this.handleError.bind(this)));
  }

  // ** ---------------- Helpers ------------------ ** //
  private getAuth(isJson: boolean = false, token = null): HttpHeaders {
    return isJson ?
      new HttpHeaders({'Content-Type': 'application/json', Authorization: token ? token : this.token}) :
      new HttpHeaders({Authorization: token ? token : this.token});
  }

  private getParams(query = {}): HttpParams {
    return Object.entries(query).reduce(
      (params, [key, value]) => params.set(key, value.toString()), new HttpParams());
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${JSON.stringify(error.error)}`);
      if (error.status === 401) {
        this.logoutAndNotify();
      }
      if (error.error.message) {
        return throwError(error.error);
      }
    }
    // return an observable with a user-facing error message
    return throwError({message: 'Something bad happened, please try again later.'});
  }
}
