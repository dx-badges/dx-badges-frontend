import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {
  logOut: boolean;

  @Output() loggedOut: EventEmitter<boolean> = new EventEmitter();
  @Output() updateUser: EventEmitter<any> = new EventEmitter();

  userLoggedOut(status: boolean): void {
    console.log('Communication: userLoggedOut');
    this.logOut = status;
    this.loggedOut.emit(this.logOut);
  }

  userProfileChanged(): void {
    console.log('Communication: userProfileChanged');
    this.updateUser.emit();
  }
}
