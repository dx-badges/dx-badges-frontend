import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private themeDark: Subject<boolean> = new Subject<boolean>();
  isThemeDark = this.themeDark.asObservable();


  private appLang: Subject<string> = new Subject<string>();
  preferredLang = this.appLang.asObservable();

  setDarkTheme(isThemeDark: boolean): void {
    this.themeDark.next(isThemeDark);
  }

  setPreferredLang(lang: string): void {
    this.appLang.next(lang);
  }

}
