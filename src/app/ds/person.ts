export interface Person {
  _id?: string;
  email: string;
  name: string;
  image?: string;
  telephone?: string;
  description?: string;
  type?: string;
  issuerOf?: string[];
}
