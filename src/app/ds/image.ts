export interface IResizeImageOptions {
  maxSize: number;
  file: File;
}
