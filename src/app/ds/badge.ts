import { Issuer } from './issuer';

export interface Badge {
  _id: string;
  name: string;
  image: string;
  criteria: string;
  alignment: string;
  tags: string[];
  expirationDate: Date;
  isDeleted: boolean;
  issuer: Issuer;
  description: string;
  num?: number;
}
