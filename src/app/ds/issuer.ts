import { Organization } from './organization';

export interface Issuer {
  _id: string;
  name: string;
  organization: Organization;
  url: string;
  image: string;
  description: string;
}
