export interface Organization {
  _id: string;
  name: string;
  url: string;
  email: string;
  image: string;
  telephone: string;
  description: string;
}
