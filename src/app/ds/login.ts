import { Person } from './person';

export interface Login {
  message: string;
  user: Person;
  token: string;
}
