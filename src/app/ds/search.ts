export interface Search {
  search?: string;
  page?: number;
  limit?: number;
  sort?: string;
  direction?: string;
}
