export interface Lang {
  value: string;
  viewValue: string;
}
