import { Person } from './person';

export interface Ping {
  user: Person;
  token: string;
}
