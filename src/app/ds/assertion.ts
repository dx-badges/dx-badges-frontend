import { Person } from './person';
import { Badge } from './badge';

export interface Assertion {
  _id: string;
  recipient: Person;
  badge: Badge;
  verification: string;
  issuedOn: Date;
  expires: Date;
  evidence: string;
  narrative: string;
  isRevoked: boolean;
  revocationReason: string;
}
