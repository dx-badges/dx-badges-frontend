import { Person } from './person';
import { Assertion } from './assertion';

export interface LylesPerson extends Person {
  schoolName?: string;
  gradeLevel?: string;
  issuedBadges?: Assertion[];
  account?: string;
}
