import { BaseComponent } from './base.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Injector } from '@angular/core';

export abstract class MainComponent extends BaseComponent {
  private SNACKBAR: MatSnackBar;

  protected constructor(protected injector: Injector) {
    super(injector);
  }

  protected openDelayedSnackBar(message: string, action: string, delay: number = 500, isSuccessError: boolean | null = null): void {
    setTimeout(() => {
      this.openSnackBar(message, action, isSuccessError);
    }, delay);
  }

  protected openSnackBar(message: string, action: string, isSuccessError: boolean | null = null): void {
    this.openTimedSnackBar(message, action, 3000, isSuccessError);
  }

  protected openTimedSnackBar(message: string, action: string, duration: number, isSuccessError: boolean | null = null): void {
    // isSuccessError = true  (Success)
    // isSuccessError = false (Error)
    // isSuccessError = n/a   (Default)
    let panelClass = [];
    // noinspection PointlessBooleanExpressionJS
    if (isSuccessError !== undefined || isSuccessError != null) {
      if (isSuccessError === true) {
        panelClass = ['snack-bar-success'];
      } else if (isSuccessError === false) {
        panelClass = ['snack-bar-error'];
      }
    }
    this.snackBar.open(message, action, {panelClass, duration});
  }

  public canUserAction(msg?: string): boolean {
    if (this.loggedIn) {
      return true;
    } else {
      if (msg) {
        this.openSnackBar('Please login to ' + msg + '.', 'Ok');
      } else {
        this.openSnackBar('Please login to continue.', 'Ok');
      }
      return false;
    }
  }

  protected get snackBar(): MatSnackBar {
    if (!this.SNACKBAR) {
      this.SNACKBAR = this.injector.get(MatSnackBar);
    }
    return this.SNACKBAR;
  }
}
