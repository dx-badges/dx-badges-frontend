import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class IsNotAuthGuard implements CanActivate {

  constructor(private router: Router,
              private api: ApiService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const loggedOut = this.api.loggedOut;
    if (!loggedOut) {
      this.router.navigate(['/']).catch(console.error);
    }
    return loggedOut;
  }

}
