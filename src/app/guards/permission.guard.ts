import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiService } from '../services/api.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionGuard implements CanActivate {

  constructor(private router: Router,
              private api: ApiService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const routeRoles = next.data.roles as Array<string>;
    const redirectTo = next.data.redirectTo as Array<string>;

    return new Promise(isOk => {
      this.api.me.subscribe(res => {
        const isAdmin = res.user && routeRoles.indexOf(res.user.type) >= 0;
        if (!isAdmin) {
          this.router.navigate(redirectTo).catch(console.error);
        }
        return isOk(isAdmin);
      });
    });
  }
}
