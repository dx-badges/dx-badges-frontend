import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'language'
})
export class LanguagePipe implements PipeTransform {

  transform(value: string, ...args: string[]): string {
    const arr = value.split(/(\[en]|\[es]|\[hm])/g);
    const i = arr.indexOf(args[0]);
    if (i >= 0 && arr.length - 1 >= i + 1) {
      return arr[i + 1];
    } else if (arr.length > 0) {
      return arr[0];
    }
    return value;
  }

}
