import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs';
import { AppService } from './services/app.service';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CookieService } from './services/cookie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private appThemeSub: Subscription;
  // private appLangSub: Subscription;

  constructor(private appService: AppService,
              private overlay: OverlayContainer,
              private renderer: Renderer2,
              private cookie: CookieService) {
  }

  ngOnInit(): void {
    if (this.cookie.getCookieBool('darkTheme')) {
      this.overlay.getContainerElement().classList.add('dark-theme');
      document.body.classList.add('dark-theme');
    }
    this.appThemeSub = this.appService.isThemeDark.subscribe(isDark => this.toggleTheme(isDark));
  }

  ngOnDestroy(): void {
    this.appThemeSub.unsubscribe();
  }

  toggleTheme(isDark): void {
    if (isDark) {
      this.overlay.getContainerElement().classList.add('dark-theme');
      this.renderer.addClass(document.body, 'dark-theme');
    } else {
      this.overlay.getContainerElement().classList.remove('dark-theme');
      this.renderer.removeClass(document.body, 'dark-theme');
    }
    this.cookie.setCookie({name: 'darkTheme', value: isDark});
  }
}
